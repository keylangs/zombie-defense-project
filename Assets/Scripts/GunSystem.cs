using UnityEngine;
using TMPro;

public class GunSystem : MonoBehaviour
{
    public GameObject bullet;
    public float shootForce, upwardForce;
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    int bulletsLeft, bulletsShot;
    public Rigidbody playerRb;
    public float playerKickbackForce;
    bool shooting, readyToShoot, reloading;
    public Camera fpsCam;
    public Transform attackPoint;
    public GameObject muzzleFlash;
    public TextMeshProUGUI ammunitionDisplay;
    public bool allowInvoke = true;
    public Animator gunAnimation;
    public AudioSource reloadAudioSource;

    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;

        if (reloadAudioSource == null)
        {
            reloadAudioSource = GetComponent<AudioSource>();
        }
    }

    private void Update()
    {
        MyInput();

        if (ammunitionDisplay != null)
        {
            ammunitionDisplay.SetText((bulletsLeft / bulletsPerTap) + " / " + (magazineSize / bulletsPerTap));
        }

        if (playerRb != null && gunAnimation != null)
        {
            if (playerRb.velocity.magnitude > 0.01f)
            {
                gunAnimation.SetBool("Moving", true);
            }
            else
            {
                gunAnimation.SetBool("Moving", false);
            }
        }
    }

    private void MyInput()
    {
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();

        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = 0;
            Shoot();
        }
    }

    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        Vector3 targetPoint;

        if (Physics.Raycast(ray, out hit))
            targetPoint = hit.point;
        else
            targetPoint = ray.GetPoint(75);

        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0);

        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
        if (currentBullet != null)
        {
            currentBullet.transform.forward = directionWithSpread.normalized;

            Rigidbody bulletRb = currentBullet.GetComponent<Rigidbody>();
            if (bulletRb != null)
            {
                bulletRb.AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
                bulletRb.AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);
            }

            if (muzzleFlash != null)
            {
                Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);
            }
        }

        bulletsLeft--;
        bulletsShot++;

        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }

        playerRb?.AddForce(-directionWithSpread.normalized * playerKickbackForce, ForceMode.Impulse);

        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
        {
            Invoke("Shoot", timeBetweenShots);
        }

        if (gunAnimation != null)
        {
            gunAnimation.SetBool("Shooting", true);
        }
    }

    private void ResetShot()
    {
        readyToShoot = true;
        allowInvoke = true;
        gunAnimation?.SetBool("Shooting", false);
    }

    private void Reload()
    {
        reloading = true;
        if (reloadAudioSource != null)
        {
            reloadAudioSource.Play();
        }
        Invoke("ReloadFinished", reloadTime);

        gunAnimation?.SetBool("Reloading", true);
    }

    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        reloading = false;

        gunAnimation?.SetBool("Reloading", false);
    }
}
