using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float moveSpeed;
    public float rotationSpeed;
    public float verticalSpeed;

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 moveDirection = new Vector3(horizontalInput, 0, verticalInput);
        moveDirection.Normalize();

        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);

        float verticalMovement = 0f;

        if (Input.GetKey("e"))
        {
            verticalMovement = 1f;
        }
        else if (Input.GetKey("q"))
        {
            verticalMovement = -1f;
        }

        Vector3 verticalMoveDirection = new Vector3(0, verticalMovement, 0);
        verticalMoveDirection.Normalize();

        transform.Translate(verticalMoveDirection * verticalSpeed * Time.deltaTime);

        if (Input.GetMouseButton(1))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            float newRotationX = transform.eulerAngles.x - mouseY * rotationSpeed;
            float newRotationY = transform.eulerAngles.y + mouseX * rotationSpeed;

            newRotationX = Mathf.Clamp(newRotationX, -50, 100);

            transform.rotation = Quaternion.Euler(newRotationX, newRotationY, 0);
        }
    }
}
