﻿using UnityEngine;

public class CustomBullet : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject explosion;
    public LayerMask whatIsEnemies;

    [Range(0f, 1f)]
    public float bounciness;
    public bool useGravity;

    public int explosionDamage;
    public float explosionRange;
    public float explosionForce;

    public int maxCollisions;
    public float maxLifetime;
    public bool explodeOnTouch = true;

    public bool shouldHome;
    private Transform target; // The target to home towards (e.g., player's transform)

    GameObject playerObject; // Reference to the player GameObject

    int collisions;
    PhysicMaterial physics_mat;

    private Vector3 lastTargetPosition; // Track the last known position of the target

    public float homingSpeed = 5f; // Adjust this value to control how quickly the bullet homes in on the target

    private void Start()
    {
        playerObject = GameObject.FindGameObjectWithTag("Player"); // Find the player object
        if (playerObject != null)
        {
            target = playerObject.transform; // Set the player's transform as the target
        }
        Setup();
    }

    private void Update()
    {
        if (collisions > maxCollisions) Explode();

        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0) Explode();
    }

    private void FixedUpdate()
    {
        if (shouldHome && target != null)
        {
            if (target.position != lastTargetPosition)
            {
                Vector3 direction = (target.position - transform.position).normalized;
                rb.velocity = direction * homingSpeed;

                rb.AddForce(direction * 10f, ForceMode.Force);
                transform.LookAt(target);
                lastTargetPosition = target.position;
            }
        }
    }

    private void Explode()
    {
        if (explosion != null) Instantiate(explosion, transform.position, Quaternion.identity);

        Collider[] enemies = Physics.OverlapSphere(transform.position, explosionRange, whatIsEnemies);
        for (int i = 0; i < enemies.Length; i++)
        {
            Rigidbody enemyRb = enemies[i].GetComponent<Rigidbody>();
            Health enemyHealth = enemies[i].GetComponent<Health>();

            Rigidbody playerRb = enemies[i].GetComponent<Rigidbody>();
            PlayerHealth playerHealth = enemies[i].GetComponent<PlayerHealth>();

            if (enemyRb != null)
            {
                enemyRb.AddExplosionForce(explosionForce, transform.position, explosionRange);

                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(explosionDamage);
                }
            }

            if (playerRb != null)
            {
                playerRb.AddExplosionForce(explosionForce, transform.position, explosionRange);

                if (playerHealth != null)
                {
                    playerHealth.ChangeHealth(-explosionDamage);
                }
            }
        }

        Invoke("Delay", 0.05f);
    }

    private void Delay()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile")) return;

        collisions++;

        if (collision.gameObject.CompareTag("Enemy") && explodeOnTouch) Explode();
    }

    private void Setup()
    {
        physics_mat = new PhysicMaterial();
        physics_mat.bounciness = bounciness;
        physics_mat.frictionCombine = PhysicMaterialCombine.Minimum;
        physics_mat.bounceCombine = PhysicMaterialCombine.Maximum;

        GetComponent<SphereCollider>().material = physics_mat;

        rb.useGravity = useGravity;
    }
}
