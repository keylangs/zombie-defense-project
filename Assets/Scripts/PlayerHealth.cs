using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;

    public bool canRegen = true;
    public float regenTime = 5f;
    public int regenAmount = 5;

    public TextMeshProUGUI healthText;

    void Start()
    {
        currentHealth = maxHealth;

        if (canRegen)
        {
            StartCoroutine(RegenerateHealth());
        }
    }

    void Update()
    {
        if (currentHealth <= 0)
            Die();

        UpdateHealthText();
    }

    public void ChangeHealth(int changeHealth)
    {
        currentHealth += changeHealth;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);

        UpdateHealthText();
    }

    private void UpdateHealthText()
    {
        if (healthText != null)
        {
            healthText.text = "Health: " + currentHealth.ToString();
        }
    }

    private void Die()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }

    private IEnumerator RegenerateHealth()
    {
        while (true)
        {
            yield return new WaitForSeconds(regenTime);

            if (currentHealth < maxHealth)
            {
                ChangeHealth(regenAmount);
            }
        }
    }
}
