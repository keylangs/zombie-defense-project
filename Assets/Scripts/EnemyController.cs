using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public bool useTagTarget = true;
    public string targetTag = "Player";
    public LayerMask targetLayerMask;
    public float stoppingDistance;

    public float attackRange;
    public int damageAmount;
    public float attackRate = 1f; // Attack rate in attacks per second
    private float nextAttackTime = 0f;

    public SphereCollider detectionCollider;

    private Transform currentTarget;
    private NavMeshAgent navMeshAgent;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.stoppingDistance = stoppingDistance;
        FindTarget();
    }

    private void Update()
    {
        if (currentTarget != null)
        {
            Vector3 targetPosition = currentTarget.position;
            Vector3 direction = targetPosition - transform.position;
            float distance = direction.magnitude;

            if (distance > stoppingDistance)
            {
                navMeshAgent.SetDestination(targetPosition - direction.normalized * stoppingDistance);

                if (Time.time >= nextAttackTime)
                {
                    Attack();
                    nextAttackTime = Time.time + 1f / attackRate;
                }
            }
        }
        else
        {
            FindTarget();
        }
    }

    private void Attack()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, attackRange, targetLayerMask);
        foreach (Collider col in colliders)
        {
            Health health = col.GetComponent<Health>();
            EnemyController enemyController = col.GetComponent<EnemyController>();

            if (health != null && (targetLayerMask != 0 && ((1 << col.gameObject.layer) & targetLayerMask) != 0))
            {
                health.TakeDamage(damageAmount);
                //Debug.Log("Attacking Object!");
            }
        }

        // Player damage handling
        Collider[] playerColliders = Physics.OverlapSphere(transform.position, attackRange);
        foreach (Collider playerCollider in playerColliders)
        {
            PlayerHealth playerHealth = playerCollider.GetComponent<PlayerHealth>();
            if (playerHealth != null && playerCollider.CompareTag("Player"))
            {
                playerHealth.ChangeHealth(-damageAmount);
                // Debug.Log("Attacking Player!");
            }
        }

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    private void FindTarget()
    {
        if (useTagTarget)
        {
            GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(targetTag);
            if (taggedObjects.Length > 0)
            {
                float closestDistance = Mathf.Infinity;
                foreach (GameObject obj in taggedObjects)
                {
                    float distance = Vector3.Distance(transform.position, obj.transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        currentTarget = obj.transform;
                    }
                }
            }
            else
            {
                // If there are no game objects with the specified layer, find by tag
                FindByTag();
            }
        }
        else
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 1000f, targetLayerMask);
            if (colliders.Length > 0)
            {
                float closestDistance = Mathf.Infinity;
                foreach (Collider col in colliders)
                {
                    float distance = Vector3.Distance(transform.position, col.transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        currentTarget = col.transform;
                    }
                }
            }
            else
            {
                // If there are no game objects with the specified layer, find by tag
                FindByTag();
            }
        }
    }

    private void FindByTag()
    {
        GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(targetTag);
        float closestDistance = Mathf.Infinity;
        foreach (GameObject obj in taggedObjects)
        {
            float distance = Vector3.Distance(transform.position, obj.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                currentTarget = obj.transform;
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            currentTarget = other.transform;
            //Debug.Log("Target changed");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            FindTarget();
            //Debug.Log("Target changed");
        }
    }
}
