using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGenerator : MonoBehaviour
{
    public GameObject[] floodLights;
    public GameObject[] directionalLights;

    private void OnDestroy()
    {
        EnableObjectsWithTargetTag();
        DisableObjectsWithTargetTag();
    }

    private void EnableObjectsWithTargetTag()
    {
        foreach (GameObject obj in floodLights)
        {
            if (obj != null)
            {
                obj.SetActive(true);
            }
        }
    }

    private void DisableObjectsWithTargetTag()
    {
        foreach (GameObject obj in directionalLights)
        {
            if (obj != null)
            {
                obj.SetActive(false);
            }
        }
    }
}
