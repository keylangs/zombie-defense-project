using UnityEngine;

public class SimpleSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    public float minSpawnDelay = 2f;
    public float maxSpawnDelay = 5f;

    private float timer = -5f; 
    private float spawnDelay = 0f;
    private bool canSpawn = false;

    private void Update()
    {
        if (timer < 0)
        {
            timer += Time.deltaTime;
            return; 
        }

        timer += Time.deltaTime;

        if (!canSpawn && timer >= spawnDelay)
        {
            canSpawn = true;
            SpawnObject();
        }

        if (canSpawn)
        {
            timer = 0f;
            canSpawn = false;
            SetRandomSpawnDelay();
        }
    }

    private void SpawnObject()
    {
        if (objectToSpawn != null)
        {
            Instantiate(objectToSpawn, transform.position, Quaternion.identity);
        }
        else
        {
            Debug.LogWarning("Please assign an object to spawn in the inspector.");
        }
    }

    private void SetRandomSpawnDelay()
    {
        spawnDelay = Random.Range(minSpawnDelay, maxSpawnDelay);
    }
}
