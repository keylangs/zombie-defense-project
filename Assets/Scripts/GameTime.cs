using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTime : MonoBehaviour
{
    //Yo Jacob if you pop this open, as it stands I"ve programmed
    //it much like FNAF to only show what the current hour is, with
    //some minor tweaks we can get more granular but I thought it might
    //be better to design it this way at least at the start


    //These variables are all exposed in editor to hopefully make it so we don't have to pop this script open very much
    [SerializeField] static int hoursToSurvive = 7; //how many in game hours we want them to survive
    [SerializeField] static float timeSeconds = 45 * 7; //How long we want the night to go on for in seconds
    [SerializeField] static int timeStartingHour = 11; //Time it starts at

    bool afterMidnight = false; //tell if AM or PM
    static float timePerHour = timeSeconds/hoursToSurvive; //how many seconds are equal an ingame hour
    float tempTime = timePerHour;
    int hoursSurvived = 0;

    public int timeCurrentHour = timeStartingHour; //the current in game hour
    public bool nightOver = false;

    // Update is called once per frame
    void Update()
    {
        HandleHourChange();
    }

    //HandleHourChange adds one to timeCurrentHour after timePerHour seconds have passed 
    private void HandleHourChange()
    {
        if (tempTime > 0)
        {
            tempTime -= Time.deltaTime; //temp time starts equaling time per hour so it gets reduced
        }
        else
        {
            tempTime = timePerHour; //once it reaches zero it gets reset
            timeCurrentHour++; //and the hour increases
            hoursSurvived++; //you survived for one more hour

            NightOverCheck(); //put these in here instead of update, they don't need to activate every frame
            HandleAMandPM();
            HandleTwelveHour();
        }
    }

    //Swaps AM and PM upon reaching midnight or noon
    private void HandleAMandPM()
    {
        if (timeCurrentHour > 11)
        {
            afterMidnight = !afterMidnight; //toggles the current aftermidnight
        }
    }
    
    //If you reach 12 it goes back to 1
    private void HandleTwelveHour()
    {
        if (timeCurrentHour > 12)
        {
            timeCurrentHour = 1;
        }
    }

    //Checks to see if you survied enough hours
    private void NightOverCheck()
    {
        if (hoursSurvived >= hoursToSurvive)
        {
            nightOver = true;
        }
    }
}
